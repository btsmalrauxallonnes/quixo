#include "quixowindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QuixoWindow w;
    w.show();

    return a.exec();
}
