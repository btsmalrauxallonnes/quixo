#include "piece.h"
#include <QPixmap>

Piece::Piece() :
    QGraphicsPixmapItem(QPixmap(":/croix.png")),
    x(0),
    y(0)
{
    setPos(x,y);
}

Piece::Piece(COULEUR_PIECE _couleur, int16_t _x, int16_t _y) :
    QGraphicsPixmapItem(),
    x(_x),
    y(_y)
{
    QPixmap * _pixmap;
    switch (_couleur) {
    case PIECE_NEUTRE:
        _pixmap = new QPixmap(":/neutre.png");
        break;
    case PIECE_ROND:
        _pixmap = new QPixmap(":/rond.png");
        break;
    case PIECE_CROIX:
        _pixmap = new QPixmap(":/croix.png");
        break;
    }
    setPixmap(*_pixmap);
    setPos(x,y);
}
