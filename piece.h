#ifndef PIECE_H
#define PIECE_H

#include <QGraphicsPixmapItem>

class Piece : public QGraphicsPixmapItem
{
public:
    enum COULEUR_PIECE {PIECE_NEUTRE=0, PIECE_ROND, PIECE_CROIX};
    explicit Piece();
    explicit Piece(COULEUR_PIECE _couleur, int16_t _x, int16_t _y);
//private:
    int16_t x;
    int16_t y;
};

#endif // PIECE_H
