#include "plateau.h"

Plateau::Plateau() :
    QGraphicsScene()
{
    for(int y=0; y<5; ++y){
        for(int x=0; x<5; ++x){
            pieces[x][y] = new Piece(Piece::PIECE_ROND, 50*x+20, 50*y+20);
            addItem(pieces[x][y]);
        }
    }
}
