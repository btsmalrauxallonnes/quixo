#ifndef PLATEAU_H
#define PLATEAU_H

#include <QGraphicsScene>
#include "piece.h"

class Plateau : public QGraphicsScene
{
public:
    Plateau();

private:
    Piece * pieces[5][5];
};

#endif // PLATEAU_H
