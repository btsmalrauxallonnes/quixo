#-------------------------------------------------
#
# Project created by QtCreator 2015-03-02T14:15:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = quixo
TEMPLATE = app


SOURCES += main.cpp\
        quixowindow.cpp \
    piece.cpp \
    plateau.cpp

HEADERS  += quixowindow.h \
    piece.h \
    plateau.h

FORMS    += quixowindow.ui

RESOURCES += \
    images.qrc
