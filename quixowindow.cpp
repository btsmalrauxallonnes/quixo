#include "quixowindow.h"
#include "ui_quixowindow.h"

QuixoWindow::QuixoWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QuixoWindow),
    ma_scene(new Plateau)
{    
    ui->setupUi(this);
    ui->ma_View->setScene(ma_scene);
}

QuixoWindow::~QuixoWindow()
{
    delete ui;
}

void QuixoWindow::on_pushButton_clicked()
{
    this->close();
}
