#ifndef QUIXOWINDOW_H
#define QUIXOWINDOW_H

#include <QMainWindow>
#include "plateau.h"

namespace Ui {
class QuixoWindow;
}

class QuixoWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit QuixoWindow(QWidget *parent = 0);
    ~QuixoWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::QuixoWindow *ui;
    Plateau * ma_scene;
    Piece * ma_piece;
};

#endif // QUIXOWINDOW_H
